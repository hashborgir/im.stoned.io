/*
 
 Copyright (C) 2021 Hash Borgir
 
 This file is part of D2Modder 
 
 Redistribution and use in source and binary forms, with
 or without modification, are permitted provided that the
 following conditions are met:
 
 * Redistributions of source code must retain the above
 copyright notice, this list of conditions and the
 following disclaimer.
 
 * Redistributions in binary form must reproduce the above
 copyright notice, this list of conditions and the
 following disclaimer in the documentation and/or other
 materials provided with the distribution.
 
 * This software must not be used for commercial purposes 
 * without my consent. Any sales or commercial use are prohibited
 * without my express knowledge and consent.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY! 
 
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
 CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
 INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 */

function syntaxHighlight(json) {
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		var cls = 'number';
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key';
			} else {
				cls = 'string';
			}
		} else if (/true|false/.test(match)) {
			cls = 'boolean';
		} else if (/null/.test(match)) {
			cls = 'null';
		}
		return '<span class="' + cls + '">' + match + '</span>';
	});
}


function search() {
	$.get("/ajax/uniqueitems.php?cmd=search&search=" + searchbox.value, function (data) {
		$('.uniqueitems-select').html(data)
	});
}

function capitalizeFirstLetter(string) {
	return string.charAt(0).toUpperCase() + string.slice(1);
}

$(document).ready(function () {
	$('.form-text').hide();
	$('.help').click(function () {
		// $('.form-text').slideToggle();
	});

	$('.op1').val(this.checked);
	$('.op1').change(function () {
		if (this.checked) {
			$('option[disabled="disabled"]').hide();
		} else {
			$('option[disabled="disabled"]').show();
		}
	});

	$(function () {
		$('[data-toggle="tooltip"]').tooltip()
	})

	$('.w-select').change(function () {
		$('.a-select,.m-select').each(function (i, v) {
			v.value = '';
			v.required = '';
		});

		x = $(this).find(':selected').text();
		y = document.getElementById('item');
		y.value = x;

	});

	$('.a-select').change(function () {
		$('.w-select,.m-select').each(function (i, v) {
			v.value = '';
			v.required = '';
		});


		x = $(this).find(':selected').text();
		y = document.getElementById('item');
		y.value = x;
	});
	$('.m-select').change(function () {
		$('.w-select,.a-select').each(function (i, v) {
			v.value = '';
			v.required = '';
		});


		x = $(this).find(':selected').text();
		y = document.getElementById('item');
		y.value = x;
	});

	$(".btnconfig").click(function () {
		$(".ctrl-config").hide();
		$("#loading").show();

	});

	$('.help').click(function () {
		$(".fa-help").remove();
		$(this).next().fadeToggle().focus().css({
			"position": "absolute",
			"z-index": "1000",
			"background": "#eee",
			"color": "black !important",
			"border": "1px solid #aaa",
			"width": "300px",
		})
	});

//	$('.help').prev().hover(function () {
//		$(".fa-help").remove();
//		$(this).next().next().fadeToggle().focus().css({
//			"position": "absolute",
//			"z-index": "1000",
//			"background": "#eee",
//			"color": "black !important",
//			"border": "1px solid #aaa",
//			"width": "300px",
//		})
//	});	



	$('.form-text').click(function () {
		$(this).fadeOut("slow");
	});



	$('').change(function () {
		if (this.checked) {
			$('option[disabled="disabled"]').hide();
		} else {
			$('option[disabled="disabled"]').show();
		}
	});




	// every time we change a prop dropdown, 
	// 
	// 1 send to server, 
	// 2 grab string, 
	// 3 update item display
	$('select[name^="prop"]').change(function () {
		prop = capitalizeFirstLetter($(this).attr("name"));
		val = escape(this.value);
		$.get("/ajax/uniqueitems.php?cmd=getString&prop=" + val, function (data) {
			console.log(data)
			$("." + prop).html(data.string1);
		});
	});


	// cmd = getUniqueItem
	$('.uniqueitems-select').change(function () {

		$(".item").attr("src", "/img/items/1.png");
		$(".item_debug_link").attr('href', "/ajax/uniqueitems.php?cmd=getUniqueItem&index=" + this.value)
		$.get("/ajax/uniqueitems.php?cmd=getUniqueItem&index=" + this.value, function (data) {
			debugData = (JSON.stringify(data, null, 4));
			$('.debug, .debug_preview').html(syntaxHighlight(debugData));
			props = data.props;

			// update preview
			$('.item_stats div').hide().html("<p></p>");
			invImg = "/img/items/" + data.invfile + ".png";
			$(".item").hide().attr("src", invImg);


			type = data["*type"];
			lvlreq = data["lvl req"];

			base = data.baseItemInfo;


			$('.item_stats p').append(`<li class="statindex">${data.index}</li>`);
			$('.item_stats p').append(`<li class="stattype">${type}</li>`);
			$('.item_stats p').append(`<li class="statlvlreq">Level: ${base.level}</li>`);
			$('.item_stats p').append(`<li class="statlvlreq">Level Required: ${lvlreq}</li>`);

			$('.item_stats p').append(`<li class="code" style="color:grey;">Item Code: ${base.code}</li>`);

			if (base.reqstr) {
				$('.item_stats p').append(`<li class="statlvlreq">Requires Strength: ${base.reqstr}</li>`);
			}
			if (base.reqdex) {
				$('.item_stats p').append(`<li class="statlvlreq">Required Dexterity: ${base.reqdex}</li>`);
			}
			if (base['gemsockets']) {
				if (base['gemsockets'] > 6) {
					base['gemsockets'] = 6;
				}
				$('.item_stats p').append(`<li class="statlvlreq">Maximum Sockets: <span color="#aaa">(${base.gemsockets})</span></li>`);

			}
			$.each(props, function (i, v) {

				if (v.string) {
					$('.item_stats p').append('<li class="' + i + '">' + v.string[0] + "</li>");
				}
			});



			$('.item_stats div').show();

			$.each(data, function (i, v) {
				$('*[name="' + i.replace(/\s/g, '') + '"]').val(v);
				if (i == 'code') {
					$('select[name="code[]"]').val(v);
					$('select[name="code[]"]').prop('required', '');
				}
			});
		});
	});

	$(".btnDebug").click(function () {
		$(".debug_preview").slideToggle();
	});


	// cmd = sortBy
	$('input[name="sort"]').change(function () {
		$.get("/ajax/uniqueitems.php?cmd=sortBy&sort=" + this.value, function (data) {
			$('.uniqueitems-select').html(data)
		});
	});

	// cmd = viewOnly
	$('input[name="view"]').change(function () {
		$.get("/ajax/uniqueitems.php?cmd=viewOnly&view=" + this.value, function (data) {
			$('.uniqueitems-select').html(data)
		});
	});

	searchbox = document.getElementById('search');
	searchbox.addEventListener('input', search);


	$('input[name="theme"]').change(function () {
		modname = $('input[name="modname"]').val();
		$.get("/res/css.php?theme=" + this.value + "&modname=" + modname, function (data) {
			location.reload();
		});


//		$('body').toggleClass("body-dark");
//		$('.container-top').toggleClass("container-top-dark");
//		$('input').toggleClass("input-dark");
//		$('select').toggleClass("select-dark");
//		$('option').toggleClass("option-dark");
//		$('.col-2,.col-3,.col-4').attr({"style":""});
	});



	//genDocUniqueItems
	$("#Unique .btnDocs").click(function () {
		window.open('/genDocs.php?cmd=genDocUniqueItems', '_blank');
	});











});